chmod -R g+rw,o+r .
find . -type d -exec chmod go+x {} \;
find . -executable -exec chmod go+x {} \;
chgrp -R op2-devel .
