module load intel-suite/11.1
module load mpi/intel-3.1
module load cmake/2.8.9
module load python/2.7.3
module load boost/1.51.0
module load swig/2.0.2
module load armadillo/3.4.2
module load vtk/5.10.1
module load petsc/3.3-p3-intel-11
module load scotch/5.1.12b

export FENICS_ROOT=`pwd`
mkdir -p $FENICS_ROOT/build
cd $FENICS_ROOT/build

# Get FEniCS release 1.1
wget -nc https://launchpad.net/dolfin/1.1.x/1.1.0/+download/dolfin-1.1.0.tar.gz
wget -nc https://launchpad.net/ufc/2.1.x/2.1.0/+download/ufc-2.1.0.tar.gz
wget -nc https://launchpad.net/ffc/1.1.x/1.1.0/+download/ffc-1.1.0.tar.gz
wget -nc https://launchpad.net/ufl/1.1.x/1.1.0/+download/ufl-1.1.0.tar.gz
wget -nc https://launchpad.net/fiat/1.1.x/release-1.1/+download/fiat-1.1.tar.gz
wget -nc https://launchpad.net/instant/1.1.x/1.1.0/+download/instant-1.1.0.tar.gz
# Extract
for t in *.tar.gz; do tar xzf $t; done

# Install UFL
cd $FENICS_ROOT/build/ufl-1.1.0
python setup.py install --prefix=$FENICS_ROOT
# Install FFC
cd $FENICS_ROOT/build/ffc-1.1.0
python setup.py install --prefix=$FENICS_ROOT
# Install UFC
cd $FENICS_ROOT/build/ufc-2.1.0
mkdir -p build && cd build
cmake -DCMAKE_INSTALL_PREFIX=$FENICS_ROOT ..
make && make install
# Install FIAT
cd $FENICS_ROOT/build/fiat-1.1
python setup.py install --prefix=$FENICS_ROOT
# Install instant
cd $FENICS_ROOT/build/instant-1.1.0
python setup.py install --prefix=$FENICS_ROOT
# Install DOLFIN
cd $FENICS_ROOT/build/dolfin-1.1.0
export PYTHONPATH=$FENICS_ROOT/lib/python2.7/site-packages:$PYTHONPATH
mkdir -p build && cd build
CC=mpicc CXX=mpicxx cmake -DCMAKE_INSTALL_PREFIX=$FENICS_ROOT -DUFC_DIR=$FENICS_ROOT/share/ufc \
  -DBOOST_ROOT=/apps/boost/1.51.0 -DARMADILLO_DIR=$ARMADILLO_HOME -DSCOTCH_DIR=$SCOTCH_HOME ..
make && make install
