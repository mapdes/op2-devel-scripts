#!/bin/sh

if [ -z $1 ]; then
  echo "Usage: build_pyop2 <tagname>"
  exit 1
fi

PYOP2_TAG=$1
module load pyop2/dependencies

if [ ! -d $PYOP2_TAG ]; then
  if [ $PYOP2_TAG == "master" ]; then
    # Clone Firedrake master
    git clone git@github.com:OP2/PyOP2 ${PYOP2_TAG}
  else
    # Clone PyOP2 at given PYOP2_TAG (note the prefix v)
    git clone -b v${PYOP2_TAG} git@github.com:OP2/PyOP2 ${PYOP2_TAG}
  fi
fi
# Build extension modules
make -C ${PYOP2_TAG} ext
# Set permissions
chmod -R g+rw,o+r ${PYOP2_TAG}
find ${PYOP2_TAG} -type d -exec chmod go+x {} \;
find ${PYOP2_TAG} -executable -exec chmod go+x {} \;
chgrp -R op2-devel ${PYOP2_TAG}
