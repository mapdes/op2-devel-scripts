#PBS -N build_firedrake
#PBS -l walltime=1:00:00
#PBS -q pqkelly
#PBS -l select=1:ncpus=4:mem=1800mb

# Usage: qsub -v FIREDRAKE_TAG=<tag> build_firedrake.pbs

if [ -z $FIREDRAKE_TAG ]; then
  FIREDRAKE_TAG=master
fi

module load git
module load op2/devel-modules
module load firedrake/${FIREDRAKE_TAG}

cd /home/op2-devel/firedrake
if [ ! -d $FIREDRAKE_TAG ]; then
  if [ $FIREDRAKE_TAG == "master" ]; then
    # Clone Firedrake master
    git clone git://github.com/firedrakeproject/firedrake ${FIREDRAKE_TAG}
  else
    # Clone Firedrake at given FIREDRAKE_TAG (note the prefix v)
    git clone -b v${FIREDRAKE_TAG} git://github.com/firedrakeproject/firedrake ${FIREDRAKE_TAG}
  fi
fi
# Build
(
cd ${FIREDRAKE_TAG}
if [ -x configure ]; then
  ./configure > compile.log 2>&1
  make python_build >> compile.log 2>&1
else
  make
fi
)
# Set permissions
chmod -R g+rw,o+r ${FIREDRAKE_TAG}
find ${FIREDRAKE_TAG} -type d -exec chmod go+x {} \;
find ${FIREDRAKE_TAG} -executable -exec chmod go+x {} \;
chgrp -R op2-devel ${FIREDRAKE_TAG}
